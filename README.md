# README #

This repo creates a RHEL for Edge image to deploy the SPL
 (SS8 Packaged Linux).

A 'RHEL for Edge' image is an rpm-ostree image that includes system packages to remotely install RHEL on Edge servers. There is also the option of not using the OSTree technology by using a VMDK disk.

While composing a RHEL for Edge image, we are using 'RHEL for Edge Container' (.tar) to create the OSTree commit and enbed it into an OCI container with a web server. This container serves the commit to the 'RHEL for Edge Installer' (.iso) image, which creates an installable boot ISO with a kickstart file configured to use the embedded OSTree commit. 

The RHEL for Edge Container and RHEL for Edge Installer images are suitable for non-network-based deployments. With RHEL for Edge images you can deploy RHEL on servers other than traditional datacenter. These servers include systems where processing of large amounts of data is done closest to the source where data is generated—Edge servers.

We use Image Builder (package osbuild-composer) to create customized RHEL for Edge images and the VMDK disk.



## What is in this repository? ##

* How to create OSTree images
* Steps to create an OSTree image
* Supported Image Customizations


### How do I create a OSTree image? ###

1. Create a Blueprint with the specifications you want to for Virtual Machine. You can use the UI or the command-line (CLI) interface. The CLI give us more options
2. Add packages to the list
3. Add a user with Administrative permissions
4. Create an image with type 'RHEL for Edge Container (.tar)'
5. Download the image
6. Load the image into Podman
7. Tag the image giving it a name
8. Run a container using this tagged Podman image
9. Create an 'empty' blueprint
10. Create an image with type 'RHEL for Edge Installer (.iso)
11. Download the bootable ISO installer

### Steps to create an OSTree image ###

Create a plain text file with the following contents

~~~~
name = "BLUEPRINT-NAME"
description = "LONG FORM DESCRIPTION TEXT"
version = "0.0.1"
modules = []
groups = []
~~~~

For every package that you want to be included in the blueprint, add the following lines to the file:

~~~~
[[packages]]
name = "package-name"
version = "package-version"
~~~~

Push (import) the blueprint:

~~~~
composer-cli blueprints push BLUEPRINT-NAME.toml
~~~~

To verify that the blueprint has been pushed and exists, list the existing blueprints:

~~~~
composer-cli blueprints list
~~~~

Check whether the components and versions listed in the blueprint and their dependencies are valid:

~~~~
composer-cli blueprints depsolve BLUEPRINT-NAME
~~~~

If you want to edit the blueprint:
-------------

~~~~
composer-cli blueprints save BLUEPRINT-NAME
~~~~

Edit the file:

~~~~
vi BLUEPRINT-NAME.toml
~~~~


When editing, before finishing with the edits, make sure the file is a valid blueprint:

1. Remove this line, if present:

        packages = []

2. Increase the version number

3. Check if the contents are valid TOML specifications

Save the file and close the editor. Push the blueprint into Image Builder:

~~~~
composer-cli blueprints push BLUEPRINT-NAME.toml
~~~~

To verify that the contents uploaded to Image Builder match your edits, list the contents of blueprint:

~~~~
composer-cli blueprints show BLUEPRINT-NAME
~~~~

Check whether the components and versions listed in the blueprint and their dependencies are valid

~~~~
composer-cli blueprints depsolve BLUEPRINT-NAME
~~~~

### Creating a RHEL for Edge image ###

~~~~
composer-cli compose start-ostree --ref rhel/8/x86_64/edge --url URL-OSTree-repository blueprint-name image-type
~~~~

I can ommit the --ref to use default value and no need to use --url right now, since there is not parent commit at this time

~~~~
composer-cli compose start-ostree rhel84a rhel-edge-container
~~~~

Check the image compose status (grep blueprint name)

~~~~
composer-cli compose status | grep rhel84a 
~~~~

When finished building, download the image:

~~~~
composer-cli compose image 60219270-34ac-49de-b20a-81ebeb499ab4
~~~~

Load the downloaded image into Podman

~~~~
cat ./7548416b-6f93-40bc-8a9e-77ba7acc263d-container.tar | podman load
~~~~

Show images:

~~~~
podman images
~~~~

Tag the image so you can identify it by a name:

~~~~
podman tag f0274e02bcbd localhost/rhel84a
~~~~

Show images again, notice it has a name now:

~~~~
podman images
~~~~

Run the container using the container image we prepared:

~~~~
podman run --name=rhel84a -p 8093:8080 localhost/rhel84a
~~~~

You have created an empty blueprint for RHEL for Edge image:

~~~~
vi rhel84a-rhel-edge-installer.toml
~~~~

Add this

~~~~
  name = rhel84a-rhel-edge-installer"
  description = "ss8 compose for edge installer"
  version = "0.0.1"
  modules = [ ]
  groups = [ ]
~~~~

Creating a RHEL for Edge Installer image

~~~~
composer-cli compose start-ostree --ref rhel/8/x86_64/edge --url URL-OSTree-repository blueprint-name image-type
~~~~

~~~~
composer-cli compose start-ostree --ref rhel/8/x86_64/edge --url http://10.0.165.35:8093/repo/ rhel84a-rhel-edge-installer rhel-edge-installer
~~~~

Check the image compose status (grep by blueprint name)

~~~~
composer-cli compose status | grep rhel84a-rhel-edge-installer
~~~~

### Supported Image Customizations ###

Set the image hostname

~~~~
[customizations]
hostname = "baseimage"
~~~~

User specifications for the resulting system image

~~~~
[[customizations.user]]
name = "USER-NAME"
description = "USER-DESCRIPTION"
password = "PASSWORD-HASH"
key = "PUBLIC-SSH-KEY"
home = "/home/USER-NAME/"
shell = "/usr/bin/bash"
groups = ["users", "wheel"]
uid = NUMBER
gid = NUMBER
~~~~

Replace PASSWORD-HASH with the actual password hash. Run the following:

~~~~
python3 -c 'import crypt,getpass;pw=getpass.getpass();print(crypt.crypt(pw) if (pw==getpass.getpass("Confirm: ")) else exit())'
~~~~

Group specifications for the resulting system image

~~~~
[[customizations.group]]
name = "GROUP-NAME"
gid = NUMBER
~~~~

Append a kernel boot parameter option to the defaults

~~~~
[customizations.kernel]
append = "KERNEL-OPTION"
~~~~

Set the timezone and the Network Time Protocol (NTP) servers. We are NOT setting timezone.

~~~~
[customizations.timezone]
timezone = "TIMEZONE"
ntpservers = "NTP_SERVER"
~~~~

Set the locale settings for the resulting system image

~~~~
[customizations.locale]
languages = ["LANGUAGE"]
keyboard = "KEYBOARD"
~~~~

Set the firewall for the resulting system image

~~~~
[customizations.firewall]
port = ["PORTS"]
~~~~

To customize the firewall services, review the available firewall services.

~~~~
firewall-cmd --get-services
~~~~

In the blueprint, under section customizations.firewall.service, specify the firewall services that you want to customize

~~~~
[customizations.firewall.services]
enabled = ["SERVICES"]
disabled = ["SERVICES"]
~~~~

Set which services to enable during the boot time

~~~~
[customizations.services]
enabled = ["SERVICES"]
disabled = ["SERVICES"]
~~~~

















